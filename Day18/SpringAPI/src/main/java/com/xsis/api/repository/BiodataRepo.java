package com.xsis.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.api.model.Biodata;

@Repository
public interface BiodataRepo extends JpaRepository<Biodata, Integer>{

}
