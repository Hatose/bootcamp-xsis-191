package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Message;

@Controller
public class MessageController {

	@RequestMapping("/Message/index")
	public String index() {
		return "Message/index";
	}
	
	@RequestMapping("/Message/add")
	public String add() {
		return "Message/add";
	}
	
	@RequestMapping(value="/Message/save", method = RequestMethod.POST)
	public String save(@ModelAttribute Message Item, Model model) {
		model.addAttribute("data",model);
		return "Message/save";
	}
}
