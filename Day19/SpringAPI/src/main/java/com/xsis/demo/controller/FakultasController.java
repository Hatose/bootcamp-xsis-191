package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FakultasController {
	
	@RequestMapping(value = "/fakultas", method = RequestMethod.GET)
	public String index() {
		return "fakultas/index";
	}
	
	@RequestMapping(value = "/fakultas/create", method = RequestMethod.GET)
	public String create() {
		return "fakultas/create";
	}
}
