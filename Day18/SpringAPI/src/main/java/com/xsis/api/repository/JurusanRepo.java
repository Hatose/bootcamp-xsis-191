package com.xsis.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.api.model.Jurusan;

@Repository
public interface JurusanRepo extends JpaRepository<Jurusan, Integer>{

}
