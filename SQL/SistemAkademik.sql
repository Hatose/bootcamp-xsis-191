1 SELECT a."NM_FAKULTAS", b."NM_JURUSAN" FROM "Fakultas" a join "Jurusan" b on a."KD_FAKULTAS" = b."KD_FAKULTAS";

2 select t1."NM_FAKULTAS", t2."NM_JURUSAN"
from public."Fakultas" t1 inner join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
where "NM_FAKULTAS" LIKE'%fi%';

3 select t1."NM_FAKULTAS", t2."NM_JURUSAN"
from public."Fakultas" t1 inner join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
where "NM_JURUSAN" LIKE'%ti%';

4 select t1."NM_FAKULTAS", count(t2."KD_FAKULTAS") AS "JML_JURUSAN"
from public."Fakultas" t1 left join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
group by t1."NM_FAKULTAS"

5 select t1."NM_FAKULTAS", count(t2."KD_FAKULTAS") AS "JML_JURUSAN"
from public."Fakultas" t1 left join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
group by t1."NM_FAKULTAS"
having count(t2."KD_FAKULTAS")>1;

6 select t1."NM_FAKULTAS", count(t2."KD_FAKULTAS") AS "JML_JURUSAN"
from public."Fakultas" t1 left join public."Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
group by t1."NM_FAKULTAS"
having count(t2."KD_FAKULTAS")=0;

7 select t3."NM_MK", t2."NM_JURUSAN", t1."NM_FAKULTAS"
from "Fakultas" t1 left join "Jurusan" t2 on t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
left join "Matakuliah" t3 on t2."KD_JURUSAN" = t3."KD_JURUSAN";

8 select t1."NM_JURUSAN", count(t2."KD_MK") AS "JML_MK"
from "Jurusan" t1 join "Matakuliah" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
group by "NM_JURUSAN";

9 select t1."NM_JURUSAN", count(t2."KD_JURUSAN") as "JML_MK" 
from "Jurusan" t1 
full join "Matakuliah" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
where t2."SKS" = 2
group by "NM_JURUSAN" ;

10 select t1."NM_JURUSAN", count(t2."KD_JURUSAN") as "JML_MK" 
from "Jurusan" t1 
full join "Matakuliah" t2 on t1."KD_JURUSAN" = t2."KD_JURUSAN"
where t2."SKS" = 3
group by "NM_JURUSAN" ;

11 select a."NM_FAKULTAS", b."NM_JURUSAN"
from "Fakultas" a full join "Jurusan" b on a."KD_FAKULTAS" =b."KD_FAKULTAS"
left join "Matakuliah" c on b."KD_JURUSAN" = c."KD_JURUSAN"
where "NM_MK" is null;

12 select c."NM_MK", b."NM_JURUSAN", a."NM_FAKULTAS"
from "Fakultas" a inner join "Jurusan" b on a."KD_FAKULTAS" = b."KD_FAKULTAS"
left join "Matakuliah" c on b."KD_JURUSAN" = c."KD_JURUSAN"
where c."NM_MK" like '%is'
group by "NM_MK", "NM_FAKULTAS", "NM_JURUSAN"

13 select "ALAMAT" as "NM_KOTA", count("KD_DOSEN") as "JML_DOSEN"
from "Dosen"
group by "ALAMAT";

14 select e."NM_DOSEN", c."NM_MK", b."NM_JURUSAN", a."NM_FAKULTAS", d."NM_KELAS", f."NM_RUANG"
from "Fakultas" a 
 join "Jurusan" b on a."KD_FAKULTAS" = b."KD_FAKULTAS" 
 join "Matakuliah" c on b."KD_JURUSAN" = c."KD_JURUSAN"
 join "Kelas" d on c."KD_MK" = d."KD_MK"
 join "Dosen" e on d."KD_DOSEN" = e."KD_DOSEN"
 join "Ruang" f on d."KD_RUANG" = f."KD_RUANG";
 
 15A SELECT T5."NM_DOSEN", T3."NM_MK", T2."NM_JURUSAN", T1."NM_FAKULTAS"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
JOIN "Matakuliah" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
JOIN "Kelas" T4 ON T3."KD_MK"=T4."KD_MK"
JOIN "Dosen" T5 ON T4."KD_DOSEN"=T5."KD_DOSEN";
 
 15B select t3."NM_DOSEN", COUNT(t1."KD_MK") AS "JML_MK"
from "Matakuliah" t1
join "Kelas" T2 on t1."KD_MK"=t2."KD_MK"
right join "Dosen" t3 on t2."KD_DOSEN"=t3."KD_DOSEN"
group by "NM_DOSEN";

16 SELECT AVG("KAPASITAS") AS "AVG_RUANG"
FROM "Ruang";

17 SELECT SUM("KAPASITAS") AS "SUM_KAPASITAS"
FROM "Ruang";

18 select "NM_RUANG", min("KAPASITAS")
from "Ruang"
group by "NM_RUANG", "KAPASITAS"
order by "KAPASITAS" limit 1

19 select "NM_RUANG", min("KAPASITAS")
from "Ruang"
group by "NM_RUANG", "KAPASITAS"
order by "KAPASITAS" desc limit 1

20 SELECT T2."NM_DOSEN", T4."NM_MK", T5."NM_JURUSAN", T6."NM_FAKULTAS", T1."NM_KELAS", T3."NM_RUANG"
FROM "Kelas" T1
JOIN "Dosen" T2 ON T1."KD_DOSEN"=T2."KD_DOSEN"
JOIN "Ruang" T3 ON T1."KD_RUANG"=T3."KD_RUANG"
JOIN "Matakuliah" T4 ON T1."KD_MK"=T4."KD_MK"
INNER JOIN "Jurusan" T5 ON T4."KD_JURUSAN"=T5."KD_JURUSAN"
LEFT JOIN "Fakultas" T6 ON T5."KD_FAKULTAS"=T6."KD_FAKULTAS"
INNER JOIN (SELECT MAX("KAPASITAS") AS "MAX"
FROM "Ruang") T7 ON T3."KAPASITAS" = T7."MAX";

21 SELECT T1."NIM", T1."NM_MHS", T2."NM_JURUSAN", T3."NM_FAKULTAS"
FROM "Mahasiswa" T1
JOIN "Jurusan" T2 ON T1."KD_JURUSAN"=T2."KD_JURUSAN"
JOIN "Fakultas" T3 ON T2."KD_FAKULTAS"=T3."KD_FAKULTAS";

22 SELECT T1."NM_FAKULTAS", T2."NM_JURUSAN", COUNT(T3."NIM") AS "JML_MHS"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
LEFT JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
GROUP BY "NM_FAKULTAS", "NM_JURUSAN";

23 select a."NM_FAKULTAS", b."NM_JURUSAN", count(c."NIM") as "JML_MHS" from "Fakultas" a 
join "Jurusan" b on a."KD_FAKULTAS" = b."KD_FAKULTAS"
join "Mahasiswa" c on b."KD_JURUSAN" = c."KD_JURUSAN"
group by "NM_FAKULTAS", b."NM_JURUSAN"
order by "JML_MHS" desc limit 1

24 select a."NM_FAKULTAS", b."NM_JURUSAN", count(c."NIM") as "JML_MHS" from "Fakultas" a 
join "Jurusan" b on a."KD_FAKULTAS" = b."KD_FAKULTAS"
join "Mahasiswa" c on b."KD_JURUSAN" = c."KD_JURUSAN"
group by "NM_FAKULTAS", b."NM_JURUSAN"
order by "JML_MHS" asc limit 1

25 SELECT T1."NM_FAKULTAS", T2."NM_JURUSAN", COUNT(T3."NIM") AS "JML_MHS"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
LEFT JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
GROUP BY "NM_FAKULTAS", "NM_JURUSAN"
HAVING COUNT(T3."NIM")=0;

26 SELECT "ALAMAT" AS "NM_KOTA", COUNT("NIM") AS "JML_DOSEN"
FROM "Mahasiswa"
GROUP BY "ALAMAT";

27 SELECT "JK", COUNT("NIM") AS "JML_DOSEN"
FROM "Mahasiswa"
GROUP BY "JK";

28 SELECT COUNT("NIM") AS "JML_MHS"
FROM "Mahasiswa"
WHERE "NM_MHS" LIKE '%Desi%';

29 SELECT COUNT("NIM") AS "JML_MHS"
FROM "Mahasiswa"
WHERE "NM_MHS" LIKE '%Ratna%';

30 SELECT T3."NIM", T3."NM_MHS", T2."NM_JURUSAN", T1."NM_FAKULTAS", T6."NM_MK", T6."SKS", T4."NILAI"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
JOIN "KelasDetail" T4 ON T3."NIM"=T4."NIM"
JOIN "Kelas" T5 ON  T4."KD_KELAS"=T5."KD_KELAS"
JOIN "Matakuliah" T6 ON T5."KD_MK"=T6."KD_MK";

31 SELECT T3."NIM", T3."NM_MHS", T2."NM_JURUSAN", T1."NM_FAKULTAS", T6."NM_MK", T6."SKS", T7."BOBOT" AS "BOBOT_NILAI"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
JOIN "KelasDetail" T4 ON T3."NIM"=T4."NIM"
JOIN "Kelas" T5 ON  T4."KD_KELAS"=T5."KD_KELAS"
JOIN "Matakuliah" T6 ON T5."KD_MK"=T6."KD_MK"
JOIN "BobotNilai" T7 ON T4."NILAI"=T7."KD_NILAI";

32 SELECT T3."NIM", T3."NM_MHS", T2."NM_JURUSAN", T1."NM_FAKULTAS", T6."NM_MK", (T6."SKS"*T7."BOBOT") AS "BOBOT_NILAI"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
JOIN "KelasDetail" T4 ON T3."NIM"=T4."NIM"
JOIN "Kelas" T5 ON  T4."KD_KELAS"=T5."KD_KELAS"
JOIN "Matakuliah" T6 ON T5."KD_MK"=T6."KD_MK"
JOIN "BobotNilai" T7 ON T4."NILAI"=T7."KD_NILAI";

33 SELECT T3."NIM", T3."NM_MHS", T2."NM_JURUSAN", T1."NM_FAKULTAS", SUM(T6."SKS"*T7."BOBOT")/SUM(T6."SKS") AS "IPK"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
JOIN "KelasDetail" T4 ON T3."NIM"=T4."NIM"
JOIN "Kelas" T5 ON  T4."KD_KELAS"=T5."KD_KELAS"
JOIN "Matakuliah" T6 ON T5."KD_MK"=T6."KD_MK"
JOIN "BobotNilai" T7 ON T4."NILAI"=T7."KD_NILAI"
GROUP BY T3."NIM", T2."NM_JURUSAN", T1."NM_FAKULTAS";

34 SELECT T3."NIM", T3."NM_MHS", T2."NM_JURUSAN", T1."NM_FAKULTAS", T6."NM_MK", T6."SKS", T4."NILAI"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
JOIN "KelasDetail" T4 ON T3."NIM"=T4."NIM"
JOIN "Kelas" T5 ON  T4."KD_KELAS"=T5."KD_KELAS"
JOIN "Matakuliah" T6 ON T5."KD_MK"=T6."KD_MK"
WHERE T4."STATUS"='LULUS';

35 SELECT T3."NIM", T3."NM_MHS", T2."NM_JURUSAN", T1."NM_FAKULTAS", T6."NM_MK", T6."SKS", T4."NILAI"
FROM "Fakultas" T1
JOIN "Jurusan" T2 ON T1."KD_FAKULTAS"=T2."KD_FAKULTAS"
JOIN "Mahasiswa" T3 ON T2."KD_JURUSAN"=T3."KD_JURUSAN"
JOIN "KelasDetail" T4 ON T3."NIM"=T4."NIM"
JOIN "Kelas" T5 ON  T4."KD_KELAS"=T5."KD_KELAS"
JOIN "Matakuliah" T6 ON T5."KD_MK"=T6."KD_MK"
WHERE T4."STATUS"='MENGULANG';