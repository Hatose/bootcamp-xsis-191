package hackkerrankimplement;

public class DiagonaDifference {

	static int diagonalDifference(int[][] arr) {
		int a=0;
		int b=0;
		for (int i = 0; i < arr.length; i++) {
			a += arr[i][i];
			b += arr[arr.length-1-i][i]; 
		}
		if(b>a) {
			return b-a;
		}
		else {
			return a-b;
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[][] arr = {{11,2,4},{4,5,6},{10,8,-12}};
		
		System.out.println(diagonalDifference(arr));
	}

}
