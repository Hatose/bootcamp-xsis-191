package tes;

import java.util.Arrays;
import java.util.Scanner;

public class Tes07 {

	static Scanner scan;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		scan = new Scanner(System.in);
		int[] a= new int[10];
		for (int i = 0; i <a.length ; i++) {
			a[i]= scan.nextInt();
		}
		
		System.out.println(getMean(a));
		System.out.println(getMedian(a));
		System.out.println(getMode(a));
		
	}

	static double getMean(int[] a) {
        double mean = 0;
        int sum = 0;
        int size = a.length;
        for(int i : a) {
            sum += i;
        }
        mean = (double) sum/size;
        return mean;
    }
	
	static double getMedian(int[] a) {
        double median = 0;
        int size = a.length;
        int [] copy = a.clone();
        Arrays.sort(copy);
        if(size % 2 == 0)
            median = (double) (copy[size/2 - 1] + copy[size/2]) / 2;
        else {
            median = (double) copy[(size-1)/2];
        }
        return median;
    }
	
	static int getMode(int[] a) {
        int mode = 0;
        int size = a.length;
        int [] copy = a.clone();
        Arrays.sort(copy);
        int count = 0, max = 0;
        int current = copy[0];
        for(int i = 0;i < size;i++) {
            if (copy[i] == current) {
                count++;
            } else {
                count = 1;
                current = copy[i];
            }
            if (count > max) {
                max = count;
                mode = copy[i];
            }
        }
        return mode;
    }
}
