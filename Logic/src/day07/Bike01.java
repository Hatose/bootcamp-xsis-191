package day07;
//buat abstract class
public abstract class Bike01 {

	//blueprint untuk child class semua harus dimasukkan
	public abstract void run();
	public abstract void component();
	public abstract void fuel();
	public abstract int speed();
}
