package day01;

public class CaseStudy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		soal8();

	}
	//1
	public static void soal1() {
		//1 3 5 7 9 11 13
		int n = 7;
		int a = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(a+" ");
			a=a+2;
		}
	}
	public static void soal1a() {
		int n =7;
		int a =1;
		for (int i = 0; i < n; i++) {
			System.out.println(a);
			a=a+2;
		}
	}
	public static void soal1b() {
		int n =7;
		int b =1;
		for (int i = 0; i < n; i++) {
			System.out.println(b);
			b=b+2;
		}
	}
	public static void soal1c() {
		int n =7;
		int c = 1;
		for (int i = 0; i < n; i++) {
			System.out.println(c);
			c=c+2;
		}
	}
	public static void soal1d() {
		int n =7;
		int d=1;
		for (int i = 0; i < n; i++) {
			System.out.println(d);
			d +=2;
		}
	}
	public static void soal1e() {
		int n =7 ;
		int e=1;
		for (int i = 0; i < n; i++) {
			System.out.println(e);
			e= e+2;
		}
	}
	public static void soal1f() {
		int n=7;
		int f=1;
		for (int i = 0; i < n; i++) {
			System.out.println(f);
			f=f+2;
		}
	}
	public static void soal1g() {
		int n=7;
		int g =1 ;
		for (int i = 0; i < n; i++) {
			System.out.println(g);
			g = g + 2;
		}
	}
	//2
	public static void soal2() {
		//2 4 6 8 10 12 14
		int n = 7;
		int a = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(a+ " ");
			a=a+2;
		}
	}
	public static void soal2a() {
		int n =7;
		int b=2;
		for (int i = 0; i < n; i++) {
			System.out.println(b);
			b=b+2;
		}
	}
	public static void soal2b() {
		int n=7;
		int bb = 2;
		for (int i = 0; i < n; i++) {
			System.out.println(bb);
			bb=bb+2;
		}
	}
	public static void soal2c() {
		int n=7;
		int c = 2;
		for (int i = 0; i < n; i++) {
			System.out.println(c);
			c=c+2;
		}
	}
	public static void soal2d() {
		int n =7;
		int d = 2;
		for (int i = 0; i < n; i++) {
			System.out.println(d);
			d=d+2;
		}
	}
	public static void soal2e() {
		int n =7;
		int e= 2;
		for (int i = 0; i < n; i++) {
			System.out.println(e);
			e=e+2;
		}
	}
	public static void soal2f() {
		int n=7;
		int f=2;
		for (int i = 0; i < n; i++) {
			System.out.println(f);
			f=f+2;
		}
	}
	public static void soal2g() {
		int n=7;
		int g=2;
		for (int i = 0; i < n; i++) {
			System.out.println(g);
			g=g+2;
		}
	}
	//3
	public static void soal3() {
		//1 4 7 10 13 16 19
		int n = 7;
		int a= 1;
		for (int i = 0; i<n; i++) {
			System.out.println(a);
			a=a+3;
		}
	}
	public static void soal3b() {
		int n =7;
		int b =1;
		for (int i = 0; i < n; i++) {
			System.out.println(b);
			b=b+3;
		}
	}
	public static void soal3c() {
		int n=7;
		int c=1;
		for (int i = 0; i < n; i++) {
			System.out.println(c);
			c=c+3;
		}
	}
	public static void soal3d() {
		int n=7;
		int d= 1;
		for (int i = 0; i < n; i++) {
			System.out.println(d);
			d=d+3;
		}
	}
	public static void soal3e() {
		int n=7;
		int e=1;
		for (int i = 0; i < n; i++) {
			System.out.println(e);
			e=e+3;
		}
	}
	public static void soal3f() {
		int n=7;
		int f = 1;
		for (int i = 0; i < n; i++) {
			System.out.println(f);
			f=f+3;
		}
	}
	public static void soal3g() {
		int n =7;
		int g=1;
		for (int i = 0; i < n; i++) {
			System.out.println(g);
			g=g+3;
		}
	}
	//4
	public static void soal4() {
		int n = 7;
		int a = 1;
		for (int i = 0; i < n; i++) {
			System.out.print(a);
			a=a+3;
		}
	}
	public static void soal4b() {
		int n=7;
		int b =1;
		for (int i = 0; i < n; i++) {
			System.out.println(b);
			b=b+3;
		}
	}
	public static void soal4c() {
		int n=7;
		int c=1;
		for (int i = 0; i < n; i++) {
			System.out.println(c);
			c=c+3;
		}
	}
	public static void soal4d() {
		int n=7;
		int d=1;
		for (int i = 0; i < n; i++) {
			System.out.println(d);
			d=d+3;
		}
	}
	public static void soal4e() {
		int n =7;
		int e=1;
		for (int i = 0; i < n; i++) {
			System.out.println(e);
			e=e+3;
		}
	}
	public static void soal4f() {
		int n=7;
		int f=1;
		for (int i = 0; i < n; i++) {
			System.out.println(f);
			f=f+3;
		}
	}
	public static void soal4g() {
		int n =7;
		int g=1;
		for (int i = 0; i < n; i++) {
			System.out.println(g);
			g=g+3;
		}
	}
	//5
	public static void soal5() {
		int n =7;
		int a=1;
		
		for (int i = 0; i < n; i++) {
			if (i==2 || i==5) {
			System.out.print("* ");
			}
			else if (i!=2 || i!=5){
			System.out.print(a+ " ");
			a=a+4;
			}
		}
	}
	public static void soal5a() {
		int n=7;
		int a=1;
		for (int i = 0; i < n; i++) {
			if(i==2 || i==5) {
				System.out.println("*");
			}
			else {
				System.out.println(a);
				a=a+4;
			}
		}
	}
	//6
	public static void soal6() {
		int n = 7;
		int a=1;
		for (int i = 0; i < n; i++) {
			if(i==2|| i==5) {
				System.out.print("* ");
			}
			else if(i!=2 || i!=5) {
				System.out.print(a+" ");
			}
			a=a+4;
		}
	}
	
	//7
	public static void soal7() {
		int n = 7;
		int a = 2;
		for (int i = 0; i < n; i++) {
			System.out.print(a+ " ");
			a=a*2;
		}
	}
	//8
	public static void soal8() {
		int n = 7;
		int a= 3;
		for (int i = 0; i < n; i++) {
			System.out.print(a+ " ");
			a=a*3;
		}
	}
	//9
	public static void soal9(){
		int n = 7;
		int a=4;
		for (int i = 0; i < n; i++) {
		if(i==2 || i==5) {
			System.out.print("* ");
		}
		else if(i!=2 || i!=5) {
			System.out.print(a + " ");
			a=a*4;	
		}
		}
	}
	//10
	public static void soal10() {
		int n = 7;
		int a = 3;
		for (int i = 0; i < n; i++) {
			if(i==3) {
			System.out.print("xxx ");
			}
			else if (i!=3) {
			System.out.print(a+ " ");
			}
			a=a*3;
		}
	}

}
