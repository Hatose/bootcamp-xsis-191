package hackkerrankimplement;

public class AVeryBigSum {

	//1
	static long bigSum(long[] ar) {
		long a= 0;//initial value
		for (int i = 0; i < ar.length; i++) {//ulang sampai ar habis
			a += ar[i];//tambah a ke i
		}
		return a;//return long[] array
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//variable=identifier
		long[] ar = {1,2,3,4,5};
		long a = bigSum(ar);
		System.out.println(a);
	}
	
	//2
	static long bigSum1(long[] ar) {
		long b=0;
		for (int i = 0; i < ar.length; i++) {
			b = b + ar[i];
		}
		return b;
	}
	public static void notmain1() {
		long b[]= {99,6,46,54,546,66,54656,66};
		long result = bigSum1(b);
		System.out.println(result);
	}

	//3
	static long bigSum2(long ar[]) {
		long c= 0;
		for (int i = 0; i < ar.length; i++) {
			c =+ ar[i];
		}
		return c;
	}
	public static void notamin3() {
		long c[] = {1,5,3,2,23,5,11};
		long hasil = bigSum2(c);
		System.out.println(hasil);
	}
	
	//4
	static long biSum3(long ar[]) {
		long d = 0;
		for (int i = 0; i < ar.length; i++) {
			d += ar[i];
		}
		return d;
	}
	public static void notmain4() {
		long[] e = {1,2,3,4,4,45,12,2};
		long result = biSum3(e);
		for (int i = 0; i < e.length; i++) {
			System.out.println(result);
		}
	}
	
	//5
	static long bigSum4(long ar[]) {
		long e= 0;
		for (int i = 0; i < ar.length; i++) {
			e=e+ar[i];
		}
		return e;
	}
	public static void notmain5() {
		long[] f = {7,4,323,666,312,623};
		long hasil=bigSum4(f);
		System.out.println(hasil);
	}
	
}
