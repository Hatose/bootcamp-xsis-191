package day07;

import java.util.ArrayList;
import java.util.Scanner;

class Student{}
class Rocker{}
class Hacker{}

public class InstanceOf {

	static String count(ArrayList mylist) {
		int a=0;
		int b =0;
		int c=0;
		for (int i = 0; i < mylist.size(); i++) {
			Object element = mylist.get(i);
			if(element instanceof Student)
				a++;
			if(element instanceof Rocker)
				b++;
			if(element instanceof Hacker)
				c++;
		}
		String ret = Integer.toString(a)+" "+Integer.toString(b)+" "+ Integer.toString(c);
		return ret;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList mylist = new ArrayList();
		Scanner sc = new Scanner(System.in);
		int t= 5;
		for (int i = 0; i < t; i++) {
			String s=sc.next();
			if(s.equals("Student"))
				mylist.add(new Student());
			if(s.equals("Rockstar"))
				mylist.add(new Rocker());
			if(s.equals("Hacker"))
				mylist.add(new Hacker());
		}
		System.out.println(count(mylist));
	}

}
