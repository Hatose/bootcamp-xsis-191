package com.xsis.masterdetail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMasterDetailApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMasterDetailApplication.class, args);
	}

}
