package day05;

public class Orang {
	//property
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	//constructor=>nama harus sama dengan class
	//method yang tidak ada tipe datanya
	//public Orang() {}
	
	//constructor
	public Orang(int id, String nama, String alamat, String jk, int umur) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk=jk;
		this.umur=umur;
	}
	
	//overloading-> nama method sama parameter beda
	//constructor-> method yang dipanggil saat instansiasi
	//dipanggil saat proses extends
	public Orang(int id, String nama, String alamat) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
	}
	
	public void showData() {
		System.out.println(id);
		System.out.println(nama);
		System.out.println(alamat);
		System.out.println(jk);
		System.out.println(umur);
		
		
	}
	
}
