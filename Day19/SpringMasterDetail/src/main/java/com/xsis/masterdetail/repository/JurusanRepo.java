package com.xsis.masterdetail.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.masterdetail.model.JurusanModel;


@Repository
public interface JurusanRepo extends JpaRepository<JurusanModel, Long>{

}
