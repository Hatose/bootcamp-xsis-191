package com.xsis.demo.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Jurusan;
import com.xsis.demo.repository.JurusanRepo;

@Controller
public class ApiJurusanController {

	@Autowired
	private JurusanRepo repo;
	
	private Log log= LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/jurusan/", method=RequestMethod.GET)
	public ResponseEntity<List<Jurusan>> list(){
		ResponseEntity<List<Jurusan>> hasil=null;
		try {
			List<Jurusan> list=repo.findAll();
			hasil= new ResponseEntity<List<Jurusan>>(list, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil= new ResponseEntity<List<Jurusan>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	@RequestMapping(value="/api/jurusan/", method= RequestMethod.POST)
	public ResponseEntity<Jurusan> create(@RequestBody Jurusan item){
		ResponseEntity<Jurusan> hasil=null;
		
		try {
			repo.save(item);
			hasil = new ResponseEntity<Jurusan>(HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil=new ResponseEntity<Jurusan>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	public ResponseEntity<Jurusan> delete(@PathVariable(name="id")Integer id){
		ResponseEntity<Jurusan> hasil = null;
		try {
			Jurusan item= repo.findById(id).orElse(null);
			repo.delete(item);
			hasil=new ResponseEntity<Jurusan>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil=new ResponseEntity<Jurusan>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	
}
