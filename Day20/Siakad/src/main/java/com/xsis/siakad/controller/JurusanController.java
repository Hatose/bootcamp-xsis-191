package com.xsis.siakad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.siakad.model.JurusanModel;
import com.xsis.siakad.repository.JurusanRepo;

@Controller
public class JurusanController {

	@Autowired
	private JurusanRepo repo;
	
	@RequestMapping(value = "/jurusan", method = RequestMethod.GET)
	public String index(Model model) {
		List<JurusanModel> data = repo.findAll();
		model.addAttribute("listdata", data);
		return "jurusan/index";
	}
	
	@RequestMapping(value = "/jurusan/add")
	public String add() {
		return "jurusan/add";
	}
	
	@RequestMapping(value = "/jurusan/save", method = RequestMethod.POST)
	public String save(@ModelAttribute JurusanModel item) {
		repo.save(item);
		return "redirect:/jurusan/index";
	}
	
	@RequestMapping(value = "/jurusan/edit/{id}")
	public String edit(Model model, @PathVariable(name="id")Integer id) {
		JurusanModel item= repo.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "jurusan/edit";
	}
	
	@RequestMapping(value = "/jurusan/delete/{id}")
	public String delete(@PathVariable(name = "id") Integer id) {
		JurusanModel item = repo.findById(id).orElse(null);
		if(item!=null) {
			repo.delete(item);
		}return "redirect:/jurusan/index";
	}
}
