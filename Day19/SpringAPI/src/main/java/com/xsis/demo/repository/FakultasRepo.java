package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.Fakultas;

@Repository
public interface FakultasRepo extends JpaRepository<Fakultas, Integer> {

}
