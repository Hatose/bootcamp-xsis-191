package hackkerrankimplement;

import java.util.Scanner;

public class GradingStudents {

	//1
	public static int[] gradingStudents(int[] grades) {
		for (int i = 0; i < grades.length; i++) {//ulang seluruh nilai grades
			if (grades[i]>38) {// jika nilai>38
				if((5-grades[i]%5)<3) {//jika hasil mod <3
					grades[i]=grades[i]+(5-grades[i]%5);//tambah nilai dari sisa grades
				}
			}
		}
		return grades;//return int[] array
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = {99,85,44,33,26,89};//nilai 
		int[] result = gradingStudents(a);//masukkan method gradingStudents ke result
		for (int i = 0; i < a.length; i++) {//ulang sampai semua a habis
			System.out.println(result[i]);//print result
		}
	}
	
	//2
	public static int[] gradingStudents1(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if(grades[i]>38) {
				if((5-grades[i] % 5)<3) {
					grades[i]= grades[i]+(grades[i]%5);
				}
			}
		}
		return grades;
	}
	public static void notmain() {
		int b[] = {11, 22, 66,13, 88, 63};
		int[] hasil = gradingStudents1(b);
		for (int i = 0; i < b.length; i++) {
			System.out.println(hasil[i]);
		}
	}
	
	//3
	public static int[] gradingStudents2(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if(grades[i]>38) {
				if((5-grades[i]%5)<3) {
					grades[i] = grades[i]+(grades[i] %5);
				}
			}
		}
		return grades;
	}
	public static void notmain2() {
		int c[] = {25, 76, 12, 56, 67, 22};
		int[] rezult = gradingStudents2(c);
		for (int i = 0; i < rezult.length; i++) {
			System.out.println(rezult[i]);
		}
	}
	
	//4
	public static int[] gradingStudents3(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if(grades[i]>38) {
				if((5-grades[i]%5)<3) {
					grades[i] = grades[i]+ grades[i]%5;
				}
			}
		}
		return grades;
	}
	public static void notmain3() {
		int d[] = {89,64,34,56,67,64,33,32,38};
		int[] hazil = gradingStudents3(d);
		for (int i = 0; i < hazil.length; i++) {
			System.out.println(hazil[i]);
		}
	}
	
	//5
	public static int[] gradingStudents4(int[] grades) {
		for (int i = 0; i < grades.length; i++) {
			if(grades[i]>38) {
				if(grades[i]%5<3) {
					grades[i] = grades[i]+ grades[i]%5;
				}
			}
		}
		return grades;
	}
	public static void notmain4() {
		int e[] = {94,6,4,77,85,22,47,75,79};
		int result[] = gradingStudents4(e);
		for (int i = 0; i < result.length; i++) {
			System.out.println(result[i]);
		}
	}

}
