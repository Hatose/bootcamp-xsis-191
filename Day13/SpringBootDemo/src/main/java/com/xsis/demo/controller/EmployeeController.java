package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Employee;
import com.xsis.demo.repository.EmployeeRepo;

@Controller
public class EmployeeController {
	// membuat auto instance dari repository
	@Autowired
	private EmployeeRepo repo;

	// request yang ada di url localhost:port/employee/index
	@RequestMapping("/employee/index")
	public String index(Model model) {
		// membuat object list Employee, kemudian diisi dari
		// object repo dengan method finAll
		List<Employee> data = repo.findAll();
		// mengirim variable listData, value nya diisi dari objct data
		model.addAttribute("listData", data);
		// menampilkan view /src/main/resources/templates
		return "Employee/index";
	}

	// request yang ada di url localhost:port/employee/add
	@RequestMapping("/employee/add")
	public String add() {
		// menampilkan view /src/main/resources/templates
		return "Employee/add";
	}

	// request yang ada di url localhost:port/employee/save
	// method nya ada post
	@RequestMapping(value = "/employee/save", method = RequestMethod.POST)
	public String save(@ModelAttribute Employee item) {
		// simpan kedatabase
		repo.save(item);
		// redirect: akan diteruskan ke halaman index
		return "redirect:/employee/index";
	}

	// request edit data
	@RequestMapping(value = "/employee/edit/{id}")
	public String edit(Model model, @PathVariable(name = "id") Long id) {
		// mengambil data dari database dengan parameter id
		Employee item = repo.findById(id).orElse(null);
		// mengirim data dari controller ke view
		model.addAttribute("data", item);
		return "Employee/edit";
	}

	// request edit data
	@RequestMapping(value = "/employee/delete/{id}")
	public String hapus(@PathVariable(name = "id") Long id) {
		// mengambil data dari database dengan parameter id
		Employee item = repo.findById(id).orElse(null);
		// remove from database
		if (item != null) {
			repo.delete(item);
		}
		return "redirect:/employee/index";
	}

}
