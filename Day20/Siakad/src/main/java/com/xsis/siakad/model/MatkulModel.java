package com.xsis.siakad.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Entity
@Table(name ="matakuliah")
@Data
public class MatkulModel {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "matkul_seq")
	@TableGenerator(name = "matkul_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private Integer id;
	
	@Column(name = "kd_matkul", nullable = false, length=10)
	private String kode;
	
	@Column(name = "nm_matkul", nullable = false, length = 150)
	private String nama;
	
	@Column(name = "sks", nullable = false)
	private Integer sks;
	
	@Column(name = "jurusan_id", nullable = false)
	private Integer jurusanId;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "jurusan_id", foreignKey = @ForeignKey(name = "fk_jurusan_matkul"), updatable = false, insertable = false)
	private JurusanModel jurusan;
	
	
}
