package day05;

public class Space {

	int id;
	String nama;
	String jenis;
	String ukuran;
	
	public Space(int id, String nama, String jenis, String ukuran) {
		this.id=id;
		this.nama = nama;
		this.jenis= jenis;
		this.ukuran=ukuran;
	}
}
