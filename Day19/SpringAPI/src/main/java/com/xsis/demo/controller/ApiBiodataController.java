package com.xsis.demo.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.Biodata;
import com.xsis.demo.repository.BiodataRepo;

@Controller
public class ApiBiodataController {

	@Autowired
	private BiodataRepo repo;
	
	private Log log= LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/biodata/",method=RequestMethod.GET)
	public ResponseEntity<List<Biodata>> list(){
		ResponseEntity<List<Biodata>> hasil=null;
		try {
			List<Biodata> list=repo.findAll();
			hasil= new ResponseEntity<List<Biodata>>(list, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil= new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	@RequestMapping(value="/api/biodata/",method=RequestMethod.POST)
	public ResponseEntity<Biodata> create(@RequestBody Biodata item){
		ResponseEntity<Biodata> hasil=null;
		try {
			repo.save(item);
			hasil = new ResponseEntity<Biodata>(item,HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil=new ResponseEntity<Biodata>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	@RequestMapping(value="/api/biodata/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Biodata> hapus(@PathVariable(name="id")Integer id){
		ResponseEntity<Biodata> hasil=null;
		try {
				Biodata item=repo.findById(id).orElse(null);
				repo.delete(item);
				hasil = new ResponseEntity<Biodata>(item, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil = new ResponseEntity<Biodata>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
	
}
