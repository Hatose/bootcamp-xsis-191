package hackkerrankimplement;

import java.util.ArrayList;
import java.util.List;

public class BirthdayChocolate {

	static int birthday(List<Integer> s, int d, int m) {
        int total=0;
        int count = 0;
        for(int i= 0; i<s.size()-m;i++){
            for(int j=0;j<m;j++){
                total = total+s.get(i+j);
            }
            if(total==d){
                count++;
            }
            total=0;
        }
        return count;
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> s = new ArrayList<Integer>();
		s.add(1);
		s.add(2);
		s.add(1);
		s.add(3);
		s.add(2);
		System.out.println(birthday(s, 3, 2));
	}

}
