package com.xsis.masterdetail.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.masterdetail.model.FakultasModel;

@Repository
public interface FakultasRepo extends JpaRepository<FakultasModel, Long>{

}
