package day04;

import java.util.Scanner;

public class JavaEndOfFile {

	static Scanner scan;

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        scan = new Scanner(System.in);
        
        for(int i = 1; scan.hasNext();i++){
            System.out.print(i+" "+scan.nextLine()+ "\n");
        }

    }
}
