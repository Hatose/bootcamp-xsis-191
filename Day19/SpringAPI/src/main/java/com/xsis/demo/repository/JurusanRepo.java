package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.Jurusan;

@Repository
public interface JurusanRepo extends JpaRepository<Jurusan, Integer> {

}
