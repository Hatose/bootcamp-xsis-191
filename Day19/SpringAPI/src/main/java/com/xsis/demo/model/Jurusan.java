package com.xsis.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="jurusan")
public class Jurusan {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator="jurusan_seq")
	@TableGenerator(name="jurusan_seq", table="tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private int id;
	
	@Column(name="kd_jurusan", nullable=false)
	private String kode;
	
	@Column(name="nm_jurusan", nullable=false)
	private String nama;
	
	@Column(name="fakultas_id", nullable=false)
	private int fakultasId;

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getFakultasId() {
		return fakultasId;
	}

	public void setFakultasId(int fakultasId) {
		this.fakultasId = fakultasId;
	}
	
	
}
