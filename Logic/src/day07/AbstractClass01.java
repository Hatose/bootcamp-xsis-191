package day07;

public class AbstractClass01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bike01 obj1= new Honda01();
		Bike01 obj2= new Yamaha01();
		obj1.run();
		obj2.run();
		
		Shape shp1 = new Rectangle();
		shp1.draw();
		Shape shp2 = new Circle();
		shp2.draw();
	}

}
