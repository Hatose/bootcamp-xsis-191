package day07;

import java.util.Scanner;

public class MinMax {

	static void miniMaxSum(int[] arr) {
        int max = arr[0];
        int min = arr[0];
        long sum = 0;
        for(int i =0; i<arr.length;i++){
            sum = sum+arr[i];
            if(arr[i]>max){
                max= arr[i];
            }
            else if(arr[i]<min){
                min = arr[i];
            }
        }
        
        System.out.print(sum-max + " ");
        System.out.println(sum-min);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}
