package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.siakad.model.JurusanModel;

@Repository
public interface JurusanRepo extends JpaRepository<JurusanModel, Integer> {

}
