package day05;

public class Game {
	int id;
	String nama;
	String genre;
	String platform;
	
	public Game(int id, String nama, String genre, String platform) {
		this.id=id;
		this.nama=nama;
		this.genre=genre;
		this.platform=platform;
	}

}
