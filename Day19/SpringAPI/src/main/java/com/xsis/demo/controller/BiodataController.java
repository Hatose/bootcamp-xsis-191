package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BiodataController {

	@RequestMapping(value = "/biodata", method = RequestMethod.GET)
	public String index() {
		
		return "biodata/index";
	}
	
	@RequestMapping(value = "/biodata/create", method = RequestMethod.GET)
	public String create() {
		return "biodata/create";
	}
}
