package day05;

public class Binatang {
	//properti
	int id;
	String nama;
	String respirasi;
	String reproduksi;
	

	//constructor
	public Binatang(int id, String nama, String respirasi, String reproduksi) {
		this.id = id;
		this.nama = nama;
		this.respirasi = respirasi;
		this.reproduksi = reproduksi;
	}
	
	public void showData() {
		System.out.println(id);
		System.out.println(nama);
		System.out.println(respirasi);
		System.out.println(reproduksi);
	}
}
