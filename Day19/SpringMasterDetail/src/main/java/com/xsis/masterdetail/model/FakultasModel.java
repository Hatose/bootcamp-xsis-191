package com.xsis.masterdetail.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="fakultas")
public class FakultasModel {

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE,generator="fakultas_seq")
	@TableGenerator(name="fakultas_seq",table="tbl_sequence", pkColumnName="seq_id", valueColumnName="seq_value", initialValue=0, allocationSize=1)
	@Column(name="id", length=10)
	private Long id;
	
	@Column(name="kd_fakultas", nullable=false, length=150)
	private String kode;
	
	@Column(name="nm_fakultas", nullable=false, length = 150)
	private String nama;

	@JsonManagedReference
	@OneToMany(mappedBy="fakultas", cascade=CascadeType.ALL)
	private List<JurusanModel> listJurusanModels = new ArrayList<JurusanModel>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public List<JurusanModel> getListJurusanModels() {
		return listJurusanModels;
	}

	public void setListJurusanModels(List<JurusanModel> listJurusanModels) {
		this.listJurusanModels = listJurusanModels;
	}
	
	
}
