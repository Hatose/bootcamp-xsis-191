package com.xsis.siakad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.siakad.model.MahasiswaModel;

@Repository
public interface MahasiswaRepo extends JpaRepository<MahasiswaModel, Integer> {

}
