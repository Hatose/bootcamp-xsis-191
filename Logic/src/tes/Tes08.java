package tes;

import java.util.Scanner;

public class Tes08 {

	static Scanner scan;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		scan = new Scanner(System.in);
		int[] arr = new int[8];
		int max = arr[0];
        int min = arr[0];
        long sum = 0;
        System.out.print("Masukkan deret angka dipisah dengan spasi : ");
        String[] arrItems = scan.nextLine().split(" ");
		
        for (int i = 0; i < 7; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }
        
        
		for(int i =0; i<arr.length;i++){
            sum += arr[i];
            if(arr[i]>max){
                max = arr[i];
            }
            else if(arr[i]<min){
                min = arr[i];
            }
        }
        
        System.out.println("hasil minimum adalah : "+ (sum-max) + " ");
        System.out.println("hasil maksimum adalah : "+(sum-min));
	}

}
